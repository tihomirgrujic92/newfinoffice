import Vue from 'vue'
import Router from 'vue-router'
import dashboard from '@/components/dashboard'
import changePass from '@/components/changePass'
import login from '@/components/login'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/changePass',
      name: 'changePass',
      component: changePass
    },
  ]
})
